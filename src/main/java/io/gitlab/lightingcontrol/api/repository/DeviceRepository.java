package io.gitlab.lightingcontrol.api.repository;

import io.gitlab.lightingcontrol.api.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<Device, String> {
}
