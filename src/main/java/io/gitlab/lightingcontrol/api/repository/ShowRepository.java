package io.gitlab.lightingcontrol.api.repository;

import io.gitlab.lightingcontrol.api.entity.Show;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShowRepository extends JpaRepository<Show, String> {
}
