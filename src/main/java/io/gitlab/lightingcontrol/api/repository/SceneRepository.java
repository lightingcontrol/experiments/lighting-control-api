package io.gitlab.lightingcontrol.api.repository;

import io.gitlab.lightingcontrol.api.entity.Scene;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SceneRepository extends JpaRepository<Scene, String> {
}
