package io.gitlab.lightingcontrol.api.repository;

import io.gitlab.lightingcontrol.api.entity.Fixture;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FixtureRepository extends JpaRepository<Fixture, String> {
}
