package io.gitlab.lightingcontrol.api.repository;

import io.gitlab.lightingcontrol.api.entity.Channel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChannelRepository extends JpaRepository<Channel, String> {
}
