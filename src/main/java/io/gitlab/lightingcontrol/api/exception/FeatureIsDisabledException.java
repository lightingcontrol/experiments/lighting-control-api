package io.gitlab.lightingcontrol.api.exception;

import java.text.MessageFormat;

public class FeatureIsDisabledException extends Exception {

    public FeatureIsDisabledException(String action, String entity) {
        super(MessageFormat.format("Feature {0} of {1} is not enabled", action, entity));
    }
}
