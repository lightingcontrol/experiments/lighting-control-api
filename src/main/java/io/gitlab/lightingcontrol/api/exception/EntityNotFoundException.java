package io.gitlab.lightingcontrol.api.exception;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(Class<?> type, Object id) {
        super("%s of %s could not be found".formatted(type, id));
    }

}
