package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.SectionDto;
import io.gitlab.lightingcontrol.api.entity.Section;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {SceneMapper.class})
public interface SectionMapper {

    SectionDto toDto(Section section);

}
