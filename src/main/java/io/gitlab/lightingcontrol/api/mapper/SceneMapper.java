package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.SceneCreateDto;
import io.gitlab.lightingcontrol.api.dto.SceneDto;
import io.gitlab.lightingcontrol.api.dto.SceneIndexDto;
import io.gitlab.lightingcontrol.api.entity.Scene;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {SceneValueMapper.class, DeviceMapper.class})
public interface SceneMapper {

    @Mapping(target = "channels", source = "allChannels")
    @Mapping(target = "devices", source = "allDevices")
    SceneIndexDto toIndexDto(Scene scene);

    SceneDto toDto(Scene scene);

    // Mapping of values is not yet available
    @Mapping(target = "allChannels", ignore = true)
    @Mapping(target = "allDevices", ignore = true)
    @Mapping(target = "values", ignore = true)
    Scene toEntity(SceneCreateDto scene);

}
