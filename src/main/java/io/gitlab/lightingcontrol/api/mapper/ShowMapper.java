package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.ShowDto;
import io.gitlab.lightingcontrol.api.dto.ShowIndexDto;
import io.gitlab.lightingcontrol.api.entity.Show;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {SectionMapper.class})
public interface ShowMapper {

    @Mapping(target = "sections", expression = "java( show.getSections().size() )")
    ShowIndexDto toIndexDto(Show show);

    ShowDto toDto(Show show);
}
