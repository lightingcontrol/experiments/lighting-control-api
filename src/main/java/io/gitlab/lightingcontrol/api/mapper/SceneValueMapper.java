package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.SceneValueDto;
import io.gitlab.lightingcontrol.api.entity.SceneValue;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ChannelMapper.class, DeviceMapper.class})
public interface SceneValueMapper {

    SceneValueDto toIndexDto(SceneValue value);

}
