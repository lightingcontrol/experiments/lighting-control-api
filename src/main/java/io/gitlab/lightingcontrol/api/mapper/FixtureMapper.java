package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.FixtureDto;
import io.gitlab.lightingcontrol.api.entity.Fixture;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ChannelMapper.class})
public interface FixtureMapper {

    FixtureDto toDto(Fixture channel);

    Fixture toEntity(FixtureDto channel);

}
