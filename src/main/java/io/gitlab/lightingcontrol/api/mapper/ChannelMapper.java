package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.ChannelDto;
import io.gitlab.lightingcontrol.api.entity.Channel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChannelMapper {

    ChannelDto toDto(Channel channel);

    default String toName(Channel channel) {
        return channel.getName();
    }

    Channel toEntity(ChannelDto channel);

}
