package io.gitlab.lightingcontrol.api.mapper;

import io.gitlab.lightingcontrol.api.dto.DeviceDto;
import io.gitlab.lightingcontrol.api.entity.Device;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {FixtureMapper.class})
public interface DeviceMapper {

    DeviceDto toDto(Device device);

    default String toName(Device device) {
        return device.getName();
    }

    Device toEntity(DeviceDto device);

}
