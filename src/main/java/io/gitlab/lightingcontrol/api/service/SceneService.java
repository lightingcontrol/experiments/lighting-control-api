package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Scene;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.SceneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class SceneService {

    private final SceneRepository repository;

    public Page<Scene> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public Optional<Scene> find(String name) {
        return repository.findById(name);
    }

    public Scene save(Scene channel) {
        return repository.save(channel);
    }

    public void deleteByName(String name) throws EntityNotFoundException {
        repository.delete(repository.findById(name).orElseThrow(() -> new EntityNotFoundException(Scene.class, name)));
    }
}
