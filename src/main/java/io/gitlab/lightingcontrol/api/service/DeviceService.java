package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Device;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class DeviceService {

    private final DeviceRepository repository;

    public Page<Device> findAll() {
        return repository.findAll(Pageable.unpaged());
    }

    public Device save(Device fixture) {
        return repository.save(fixture);
    }

    public void deleteByName(String name) throws EntityNotFoundException {
        repository.delete(repository.findById(name).orElseThrow(() -> new EntityNotFoundException(Device.class, name)));
    }
}
