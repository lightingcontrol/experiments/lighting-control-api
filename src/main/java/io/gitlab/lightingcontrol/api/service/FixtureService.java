package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Fixture;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.FixtureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class FixtureService {

    private final FixtureRepository repository;

    public Page<Fixture> findAll() {
        return repository.findAll(Pageable.unpaged());
    }

    public Fixture save(Fixture fixture) {
        return repository.save(fixture);
    }

    public void deleteByName(String name) throws EntityNotFoundException {
        repository.delete(repository.findById(name).orElseThrow(() -> new EntityNotFoundException(Fixture.class, name)));
    }
}
