package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Channel;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.ChannelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ChannelService {

    private final ChannelRepository repository;

    public Page<Channel> findAll() {
        return repository.findAll(Pageable.unpaged());
    }

    public Channel save(Channel channel) {
        return repository.save(channel);
    }

    public void deleteByName(String name) throws EntityNotFoundException {
        repository.delete(repository.findById(name).orElseThrow(() -> new EntityNotFoundException(Channel.class, name)));
    }
}
