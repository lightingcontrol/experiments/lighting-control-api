package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Scene;
import io.gitlab.lightingcontrol.api.entity.Show;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.ShowRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ShowService {

    private final ShowRepository repository;

    public Page<Show> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public Optional<Show> find(String name) {
        return repository.findById(name);
    }

    public Show save(Show channel) {
        return repository.save(channel);
    }

    public void deleteByName(String name) throws EntityNotFoundException {
        repository.delete(repository.findById(name).orElseThrow(() -> new EntityNotFoundException(Scene.class, name)));
    }
}
