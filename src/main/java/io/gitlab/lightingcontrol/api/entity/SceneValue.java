package io.gitlab.lightingcontrol.api.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
@Getter
@NoArgsConstructor
@Setter
@Table(name = "scene_value")
@ToString
public class SceneValue {

    @Id
    private UUID id;
    @ManyToOne
    @JoinColumn(name = "scene_name")
    private Scene scene;
    @JoinTable(name = "scene_value_device", joinColumns = {@JoinColumn(name = "scene_value_id")}, inverseJoinColumns = {@JoinColumn(name = "device_name")})
    @ManyToMany
    @OrderColumn(name = "ordinal")
    private List<Device> devices;
    @JoinTable(name = "scene_value_channel", joinColumns = {@JoinColumn(name = "scene_value_id")}, inverseJoinColumns = {@JoinColumn(name = "channel_name")})
    @ManyToMany
    private List<Channel> channels;
    @Column(name = "min_value")
    private Integer min;
    @Column(name = "max_value")
    private Integer max;
    private String type;
    private BigDecimal rate;
    @Column(name = "offset_of")
    private Integer offset;
    private Integer phase;

}
