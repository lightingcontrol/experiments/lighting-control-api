package io.gitlab.lightingcontrol.api.entity;


import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "name")
@Getter
@NoArgsConstructor
@Setter
@ToString
public class Fixture {

    @Id
    private String name;
    private String brand;
    @JoinTable(name = "fixture_channel", joinColumns = {@JoinColumn(name = "fixture_name")}, inverseJoinColumns = {@JoinColumn(name = "channel_name")})
    @ManyToMany
    @OrderColumn(name = "ordinal")
    private List<Channel> channels;

}