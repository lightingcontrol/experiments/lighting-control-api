package io.gitlab.lightingcontrol.api.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.*;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "name")
@Getter
@NoArgsConstructor
@Setter
@ToString
public class Device {

    @Id
    private String name;
    private Integer startAddress;
    @ManyToOne
    private Fixture fixture;

}