package io.gitlab.lightingcontrol.api.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OrderBy;
import lombok.*;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "name")
@Getter
@NoArgsConstructor
@Setter
@ToString
public class Channel {

    @Id
    @OrderBy
    private String name;
    private String type;

}