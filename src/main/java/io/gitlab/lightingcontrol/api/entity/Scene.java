package io.gitlab.lightingcontrol.api.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "name")
@Getter
@NoArgsConstructor
@Setter
@ToString
public class Scene {

    @Id
    private String name;
    private String type;
    @OneToMany(mappedBy = "scene")
    private Set<SceneValue> values;

    // This should be part of the SceneMapper, don't use it elsewhere
    public Set<Device> getAllDevices() {
        return values.stream().map(SceneValue::getDevices).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    // This should be part of the SceneMapper, don't use it elsewhere
    public Set<Channel> getAllChannels() {
        return values.stream().map(SceneValue::getChannels).flatMap(Collection::stream).collect(Collectors.toSet());
    }

}
