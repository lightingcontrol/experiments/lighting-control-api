package io.gitlab.lightingcontrol.api.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "name")
@Getter
@NoArgsConstructor
@Setter
@Table(name = "shows")
@ToString
public class Show {

    @Id
    private String name;
    @OneToMany(mappedBy = "show")
    @OrderColumn(name = "ordinal")
    private List<Section> sections;

}
