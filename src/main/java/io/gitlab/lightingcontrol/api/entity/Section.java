package io.gitlab.lightingcontrol.api.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
@Getter
@NoArgsConstructor
@Setter
@ToString
public class Section {

    @ManyToOne
    Show show;
    Short ordinal;
    String name;
    Short bpm;
    @JoinTable(name = "section_scene", joinColumns = {@JoinColumn(name = "section_id")}, inverseJoinColumns = {@JoinColumn(name = "scene_name")})
    @ManyToMany
    Set<Scene> scenes;
    @Id
    private UUID id;

}
