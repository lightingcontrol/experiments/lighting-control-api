package io.gitlab.lightingcontrol.api.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "features")
@Data
public class ConfigProperties {

    private EditingProperties editing;

    @Data
    public static class EditingProperties {
        private boolean channels;
        private boolean fixtures;
        private boolean devices;
        private boolean scenes;
        private boolean sections;
        private boolean shows;
    }

}