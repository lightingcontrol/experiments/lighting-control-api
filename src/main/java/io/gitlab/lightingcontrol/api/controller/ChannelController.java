package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.ChannelDto;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.ChannelMapper;
import io.gitlab.lightingcontrol.api.service.ChannelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/channel")
public class ChannelController {

    private final ChannelService service;
    private final ChannelMapper mapper;
    private final ConfigProperties configProperties;

    @GetMapping
    @Transactional(readOnly = true)
    public Page<ChannelDto> index() {
        return service.findAll().map(mapper::toDto);
    }

    @PutMapping
    @Transactional
    public ChannelDto create(@RequestBody ChannelDto channel) throws FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        return mapper.toDto(service.save(mapper.toEntity(channel)));
    }

    @DeleteMapping("/{name}")
    @Transactional
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String name) throws EntityNotFoundException, FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        service.deleteByName(name);
    }

}
