package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.FixtureDto;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.FixtureMapper;
import io.gitlab.lightingcontrol.api.service.FixtureService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/fixture")
public class FixtureController {

    private final FixtureService service;
    private final FixtureMapper mapper;
    private final ConfigProperties configProperties;

    @GetMapping
    @Transactional(readOnly = true)
    public Page<FixtureDto> index() {
        return service.findAll().map(mapper::toDto);
    }

    @PutMapping
    @Transactional
    public FixtureDto create(@RequestBody FixtureDto channel) throws FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        return mapper.toDto(service.save(mapper.toEntity(channel)));
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void delete(@PathVariable String name) throws EntityNotFoundException, FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        service.deleteByName(name);
    }

}
