package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.dto.InfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/info")
public class InfoController {

    @Value("${git.branch}")
    private String branch;
    @Value("${git.commit.id}")
    private String commitId;
    @Value("${git.build.version}")
    private String version;
    @Value("${git.commit.id.abbrev}")
    private String commitIdAbbr;

    @GetMapping
    public InfoDto getCommitId() {
        return new InfoDto(branch, commitId, version, commitIdAbbr);
    }
}
