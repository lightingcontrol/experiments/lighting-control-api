package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.ShowDto;
import io.gitlab.lightingcontrol.api.dto.ShowIndexDto;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.ShowMapper;
import io.gitlab.lightingcontrol.api.service.ShowService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/show")
public class ShowController {

    private final ShowService service;
    private final ShowMapper mapper;
    private final ConfigProperties configProperties;

    @GetMapping
    @Transactional(readOnly = true)
    public Page<ShowIndexDto> index(@PageableDefault(sort = {"name"}, size = 25, direction = Sort.Direction.ASC) Pageable page) {
        return service.findAll(page).map(mapper::toIndexDto);
    }

    @GetMapping("/{name}")
    @Transactional(readOnly = true)
    public ShowDto entry(@PathVariable String name) {
        return service.find(name).map(mapper::toDto).orElseThrow();
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void delete(@PathVariable String name) throws EntityNotFoundException, FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        service.deleteByName(name);
    }

}
