package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

@RestControllerAdvice
public class ErrorAdviceController {

    public static final String TYPE_NOT_FOUND = "https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found";
    public static final String TYPE_METHOD_NOT_ALLOWED = "https://www.rfc-editor.org/rfc/rfc9110.html#name-405-method-not-allowed";
    public static final String TYPE_NOT_ACCEPTABLE = "https://www.rfc-editor.org/rfc/rfc9110.html#name-406-not-acceptable";

    @ResponseStatus(HttpStatus.CONFLICT)  // 409
    @ExceptionHandler(SQLException.class)
    public ProblemDetail sqlException(SQLException exception) {
        return ProblemDetail.forStatusAndDetail(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND) // 404
    @ExceptionHandler(EntityNotFoundException.class)
    public ProblemDetail entityNotFoundException(EntityNotFoundException exception) throws URISyntaxException {
        return toProblemDetail(HttpStatus.NOT_FOUND, exception.getMessage(), TYPE_NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED) // 405
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ProblemDetail entityNotFoundException(HttpRequestMethodNotSupportedException exception) throws URISyntaxException {
        return toProblemDetail(HttpStatus.METHOD_NOT_ALLOWED, exception.getMessage(), TYPE_METHOD_NOT_ALLOWED);
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE) // 406
    @ExceptionHandler(FeatureIsDisabledException.class)
    public ProblemDetail featureIsDisabledException(FeatureIsDisabledException exception) throws URISyntaxException {
        return toProblemDetail(HttpStatus.NOT_ACCEPTABLE, exception.getMessage(), TYPE_NOT_ACCEPTABLE);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND) // 404
    @ExceptionHandler(NoHandlerFoundException.class)
    public ProblemDetail noHandlerFoundException(NoHandlerFoundException exception) throws URISyntaxException {
        return toProblemDetail(HttpStatus.NOT_FOUND, exception.getMessage(), TYPE_NOT_FOUND);
    }

    private ProblemDetail toProblemDetail(HttpStatus status, String message, String type) throws URISyntaxException {
        ProblemDetail response = ProblemDetail.forStatusAndDetail(status, message);
        response.setType(new URI(type));
        response.setProperty("frontend", "https://lightingcontrol.gitlab.io");
        response.setProperty("hint", "Try accessing through https://lightingcontrol.gitlab.io/.");
        return response;
    }
}
