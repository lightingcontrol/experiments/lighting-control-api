package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.SceneCreateDto;
import io.gitlab.lightingcontrol.api.dto.SceneDto;
import io.gitlab.lightingcontrol.api.dto.SceneIndexDto;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.SceneMapper;
import io.gitlab.lightingcontrol.api.service.SceneService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/scene")
public class SceneController {

    private final SceneService service;
    private final SceneMapper mapper;
    private final ConfigProperties configProperties;

    @GetMapping
    @Transactional(readOnly = true)
    public Page<SceneIndexDto> index(@PageableDefault(sort = {"name"}, size = 25, direction = Sort.Direction.ASC) Pageable page) {
        return service.findAll(page).map(mapper::toIndexDto);
    }

    @GetMapping("/{name}")
    @Transactional(readOnly = true)
    public SceneDto entry(@PathVariable String name) {
        return service.find(name).map(mapper::toDto).orElseThrow();
    }

    @PutMapping
    @Transactional
    public SceneDto create(@RequestBody SceneCreateDto sceneDto) throws FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        return mapper.toDto(service.save(mapper.toEntity(sceneDto)));
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void delete(@PathVariable String name) throws EntityNotFoundException, FeatureIsDisabledException {
        if (!configProperties.getEditing().isChannels()) {
            throw new FeatureIsDisabledException("editing", "channel");
        }
        service.deleteByName(name);
    }

}
