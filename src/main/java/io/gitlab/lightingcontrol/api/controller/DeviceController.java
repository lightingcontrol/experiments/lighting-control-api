package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.DeviceDto;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.DeviceMapper;
import io.gitlab.lightingcontrol.api.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/device")
public class DeviceController {

    private final DeviceService service;
    private final DeviceMapper mapper;
    private final ConfigProperties configProperties;

    @GetMapping
    @Transactional(readOnly = true)
    public Page<DeviceDto> index() {
        return service.findAll().map(mapper::toDto);
    }

    @PutMapping
    @Transactional
    public DeviceDto create(@RequestBody DeviceDto device) throws FeatureIsDisabledException {
        if (!configProperties.getEditing().isDevices()) {
            throw new FeatureIsDisabledException("editing", "device");
        }
        return mapper.toDto(service.save(mapper.toEntity(device)));
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void delete(@PathVariable String name) throws EntityNotFoundException, FeatureIsDisabledException {
        if (!configProperties.getEditing().isDevices()) {
            throw new FeatureIsDisabledException("editing", "device");
        }
        service.deleteByName(name);
    }

}
