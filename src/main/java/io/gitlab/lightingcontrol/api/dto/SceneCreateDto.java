package io.gitlab.lightingcontrol.api.dto;

public record SceneCreateDto(String name, String type) {
}
