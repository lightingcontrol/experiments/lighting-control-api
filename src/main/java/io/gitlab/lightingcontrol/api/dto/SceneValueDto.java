package io.gitlab.lightingcontrol.api.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public record SceneValueDto(
        UUID id,
        List<String> devices,
        List<String> channels,
        Integer min,
        Integer max,
        String type,
        BigDecimal rate,
        Integer offset,
        Integer phase) {
}
