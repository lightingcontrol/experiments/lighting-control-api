package io.gitlab.lightingcontrol.api.dto;

public record InfoDto(
        String branch,
        String commitId,
        String version,
        String commitIdAbbr) {
}
