package io.gitlab.lightingcontrol.api.dto;

public record ShowIndexDto(String name, Integer sections) {
}
