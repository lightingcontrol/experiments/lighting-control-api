package io.gitlab.lightingcontrol.api.dto;

import java.util.List;

public record ShowDto(String name, List<SectionDto> sections) {
}
