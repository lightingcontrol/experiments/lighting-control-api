package io.gitlab.lightingcontrol.api.dto;

import java.util.Set;

public record SceneIndexDto(String name, String type, Set<DeviceDto> devices, Set<ChannelDto> channels) {
}
