package io.gitlab.lightingcontrol.api.dto;

public record ChannelDto(String name, String type) {
}
