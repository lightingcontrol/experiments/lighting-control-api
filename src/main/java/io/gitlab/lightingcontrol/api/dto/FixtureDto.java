package io.gitlab.lightingcontrol.api.dto;

import java.util.List;

public record FixtureDto(String name, String brand, List<ChannelDto> channels) {
}
