package io.gitlab.lightingcontrol.api.dto;

import java.util.List;

public record SceneDto(String name, String type, List<SceneValueDto> values) {
}
