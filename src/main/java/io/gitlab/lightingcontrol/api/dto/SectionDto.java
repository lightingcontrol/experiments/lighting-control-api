package io.gitlab.lightingcontrol.api.dto;

import java.util.Set;
import java.util.UUID;

public record SectionDto(UUID id, String name, Short bpm, Set<SceneIndexDto> scenes) {
}
