package io.gitlab.lightingcontrol.api.dto;

public record DeviceDto(String name, Integer startAddress, FixtureDto fixture) {
}
