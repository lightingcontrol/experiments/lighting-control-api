create table device
(
    name          varchar(255) not null,
    fixture_name  varchar(255) not null,
    start_address integer      not null,
    constraint pk_device primary key (name)
);

alter table device
    add constraint fk_device_fixture foreign key (fixture_name) references fixture (name);
create index id_device_start_address on device (start_address);

insert into device (name, start_address, fixture_name)
values ('par1', 0, 'rgb'),
       ('par2', 3, 'rgb'),
       ('par3', 6, 'rgb'),
       ('par4', 9, 'rgb'),
       ('bar1', 12, 'rgbauv'),
       ('bar2', 17, 'rgbauv'),
       ('bar3', 22, 'rgbauv'),
       ('pix1', 27, 'pix8'),
       ('pix2', 51, 'pix8'),
       ('scan1', 75, 'scanrgb'),
       ('scan2', 80, 'scanrgb');
