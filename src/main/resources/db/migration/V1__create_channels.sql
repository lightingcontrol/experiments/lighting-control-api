create table channel
(
    name varchar(255) not null,
    type varchar(255) not null,
    constraint pk_channel primary key (name)
);

insert into channel (name, type)
values ('red', 'color'),
       ('green', 'color'),
       ('blue', 'color'),
       ('amber', 'color'),
       ('white', 'color'),
       ('uv', 'color'),
       ('master', 'master'),
       ('pan', 'position'),
       ('tilt', 'position'),
       ('fog', 'effect'),
       ('color', 'color'),
       ('gobo', 'effect'),
       ('strobe', 'effect');
