create table fixture
(
    name  varchar(255) not null,
    brand varchar(255) not null,
    constraint pk_fixture primary key (name)
);

create table fixture_channel
(
    fixture_name varchar(255) not null,
    ordinal      int          not null,
    channel_name varchar(255) not null,
    constraint pk_fixture_channel primary key (fixture_name, ordinal)
);

alter table fixture_channel
    add constraint fk_fixture_channel_fixture foreign key (fixture_name) references fixture (name);
alter table fixture_channel
    add constraint fk_fixture_channel_channel foreign key (channel_name) references channel (name);

insert into fixture (name, brand)
values ('rgb', 'generic'),
       ('rgba', 'generic'),
       ('rgbauv', 'generic'),
       ('rgbuv', 'generic'),
       ('rgbm', 'generic'),
       ('rgbam', 'generic'),
       ('pix8', 'generic'),
       ('scanrgb', 'moving-head');

insert into fixture_channel (fixture_name, ordinal, channel_name)
values ('rgb', 0, 'red'),
       ('rgb', 1, 'green'),
       ('rgb', 2, 'blue'),
       ('rgba', 0, 'red'),
       ('rgba', 1, 'green'),
       ('rgba', 2, 'blue'),
       ('rgba', 3, 'amber'),
       ('rgbauv', 0, 'red'),
       ('rgbauv', 1, 'green'),
       ('rgbauv', 2, 'blue'),
       ('rgbauv', 3, 'amber'),
       ('rgbauv', 4, 'uv'),
       ('rgbuv', 0, 'red'),
       ('rgbuv', 1, 'green'),
       ('rgbuv', 2, 'blue'),
       ('rgbuv', 3, 'uv'),
       ('rgbm', 0, 'red'),
       ('rgbm', 1, 'green'),
       ('rgbm', 2, 'blue'),
       ('rgbm', 3, 'master'),
       ('rgbam', 0, 'red'),
       ('rgbam', 1, 'green'),
       ('rgbam', 2, 'blue'),
       ('rgbam', 3, 'amber'),
       ('rgbam', 4, 'master'),
       ('pix8', 0, 'red'),
       ('pix8', 1, 'green'),
       ('pix8', 2, 'blue'),
       ('pix8', 3, 'red'),
       ('pix8', 4, 'green'),
       ('pix8', 5, 'blue'),
       ('pix8', 6, 'red'),
       ('pix8', 7, 'green'),
       ('pix8', 8, 'blue'),
       ('pix8', 9, 'red'),
       ('pix8', 10, 'green'),
       ('pix8', 11, 'blue'),
       ('pix8', 12, 'red'),
       ('pix8', 13, 'green'),
       ('pix8', 14, 'blue'),
       ('pix8', 15, 'red'),
       ('pix8', 16, 'green'),
       ('pix8', 17, 'blue'),
       ('pix8', 18, 'red'),
       ('pix8', 19, 'green'),
       ('pix8', 20, 'blue'),
       ('pix8', 21, 'red'),
       ('pix8', 22, 'green'),
       ('pix8', 23, 'blue'),
       ('scanrgb', 0, 'pan'),
       ('scanrgb', 1, 'tilt'),
       ('scanrgb', 2, 'red'),
       ('scanrgb', 3, 'green'),
       ('scanrgb', 4, 'blue');
