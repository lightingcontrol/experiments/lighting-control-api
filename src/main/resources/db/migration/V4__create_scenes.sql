create table scene
(
    name varchar(255) not null,
    type varchar(255) not null,
    constraint pk_scene primary key (name)
);

create table scene_value
(
    id         uuid                                                                  not null,
    scene_name varchar(255)                                                          not null,
    max_value  integer                                                               not null default 255,
    min_value  integer                                                               not null default 0,
    type       enum ('static', 'sine', 'square', 'triangle', 'ramp-up', 'ramp-down') not null default 'static',
    rate       decimal(6, 4)                                                         not null default 1,
    offset_of  integer                                                               not null default 0,
    phase      integer                                                               not null default 0,
    constraint pk_scene_value primary key (id)
);

alter table scene_value
    add constraint fk_scene_value_scene foreign key (scene_name) references scene (name);

create table scene_value_device
(
    scene_value_id uuid         not null,
    ordinal        integer      not null,
    device_name    varchar(255) not null,
    constraint pk_scene_value_device primary key (scene_value_id, device_name)
);

alter table scene_value_device
    add constraint fk_scene_value_device_device foreign key (device_name) references device (name);

create table scene_value_channel
(
    scene_value_id uuid         not null,
    channel_name   varchar(255) not null,
    constraint pk_scene_value_channel primary key (scene_value_id, channel_name)
);

alter table scene_value_channel
    add constraint fk_scene_value_channel_channel foreign key (channel_name) references channel (name);

insert into scene (name, type)
values ('blackout', 'blackout'),
       ('lux', 'lux'),
       ('color-red-all', 'color'),
       ('wave-red-all-1', 'wave');

insert into scene_value (id, scene_name, min_value, max_value, type, rate, offset_of, phase)
values ('cd281823-29d5-47b9-9dae-d30611bb179f', 'lux', 0, 255, 'static', 1, 0, 0),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 'color-red-all', 0, 255, 'static', 1, 0, 0),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 'wave-red-all-1', 0, 255, 'sine', 1, 0, 32);

insert into scene_value_device (scene_value_id, ordinal, device_name)
values ('cd281823-29d5-47b9-9dae-d30611bb179f', 0, 'par1'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 1, 'scan1'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 2, 'bar1'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 3, 'par2'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 4, 'pix1'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 5, 'bar2'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 6, 'pix2'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 7, 'par3'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 8, 'bar3'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 9, 'scan2'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 10, 'par4'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 0, 'par1'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 1, 'scan1'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 2, 'bar1'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 3, 'par2'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 4, 'pix1'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 5, 'bar2'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 6, 'pix2'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 7, 'par3'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 8, 'bar3'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 9, 'scan2'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 10, 'par4'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 0, 'par1'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 1, 'scan1'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 2, 'bar1'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 3, 'par2'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 4, 'pix1'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 5, 'bar2'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 6, 'pix2'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 7, 'par3'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 8, 'bar3'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 9, 'scan2'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 10, 'par4');

insert into scene_value_channel (scene_value_id, channel_name)
values ('cd281823-29d5-47b9-9dae-d30611bb179f', 'red'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 'green'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 'blue'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 'amber'),
       ('cd281823-29d5-47b9-9dae-d30611bb179f', 'uv'),
       ('060acda3-cf01-4119-9cf1-309c102b178a', 'red'),
       ('d9f0cf35-3597-4fe8-998b-d53fc845424b', 'red');
