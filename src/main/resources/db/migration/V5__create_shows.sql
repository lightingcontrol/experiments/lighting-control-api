create table shows
(
    name varchar(255) not null,
    constraint pk_show primary key (name)
);

create table section
(
    id        uuid         not null,
    show_name varchar(255) not null,
    ordinal   integer      not null,
    name      varchar(255) not null,
    bpm       smallint     not null,
    constraint pk_section_name primary key (id),
    constraint uk_section_show_ordinal unique (show_name, ordinal)
);

alter table section
    add constraint fk_section_show foreign key (show_name) references shows (name);

create table section_scene
(
    section_id uuid         not null,
    scene_name varchar(255) not null,
    constraint pk_section_scene primary key (section_id, scene_name)
);

alter table section_scene
    add constraint fk_section_scene_section foreign key (section_id) references section (id);
alter table section_scene
    add constraint fk_section_scene_scene foreign key (scene_name) references scene (name);

insert into shows (name)
values ('Demo');

insert into section (id, show_name, ordinal, name, bpm)
values ('c809fc9a-8c1d-3cea-b837-d1ae14a3048b', 'Demo', 0, 'Setup', 0),
       ('961a9d6f-3a88-3179-bdfe-4c74e08764db', 'Demo', 1, 'Blackout', 0),
       ('af392af0-1603-3a38-b672-689241b648b2', 'Demo', 2, 'Intro', 135),
       ('e02d9df6-c79b-3570-9ca9-96adb87fcb70', 'Demo', 3, 'Crazy Song', 126),
       ('d0e187e7-3979-3da3-87d7-456a23fec8e0', 'Demo', 4, 'Blackout', 0);

insert into section_scene (section_id, scene_name)
values ('c809fc9a-8c1d-3cea-b837-d1ae14a3048b', 'lux'),
       ('af392af0-1603-3a38-b672-689241b648b2', 'color-red-all'),
       ('e02d9df6-c79b-3570-9ca9-96adb87fcb70', 'wave-red-all-1');
