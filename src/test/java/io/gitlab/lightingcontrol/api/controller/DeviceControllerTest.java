package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.DeviceDto;
import io.gitlab.lightingcontrol.api.entity.Device;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.DeviceMapper;
import io.gitlab.lightingcontrol.api.service.DeviceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeviceControllerTest {

    @InjectMocks
    private DeviceController sut;
    @Mock
    private DeviceService service;
    @Mock
    private DeviceMapper mapper;
    @Mock
    private ConfigProperties configProperties;
    @Mock
    private ConfigProperties.EditingProperties editingProperties;

    @Test
    void indexFindAndMapsResult() {
        var device = new Device("name", 0, null);
        var deviceDto = new DeviceDto("name", 0, null);
        when(service.findAll()).thenReturn(new PageImpl<>(List.of(device)));
        when(mapper.toDto(device)).thenReturn(deviceDto);

        var result = sut.index();

        verify(service).findAll();
        verify(mapper).toDto(device);
        assertThat(result, allOf(
                hasProperty("content", hasItem(deviceDto)),
                hasProperty("size", is(1)),
                hasProperty("totalPages", is(1)),
                hasProperty("totalElements", is(1L))));
    }

    @Test
    void createWhenNotAllowedThrowsException() {
        var deviceDto = new DeviceDto("name", 0, null);
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isDevices()).thenReturn(false);

        var exception = assertThrows(FeatureIsDisabledException.class, () -> sut.create(deviceDto));

        assertThat(exception.getMessage(), is("Feature editing of device is not enabled"));
    }

    @Test
    void createWhenAllowedCallsService() throws FeatureIsDisabledException {
        var inDto = new DeviceDto("in", 0, null);
        var inEntity = new Device("in", 0, null);
        var outDto = new DeviceDto("out", 0, null);
        var outEntity = new Device("out", 0, null);
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isDevices()).thenReturn(true);
        when(mapper.toEntity(inDto)).thenReturn(inEntity);
        when(mapper.toDto(outEntity)).thenReturn(outDto);
        when(service.save(inEntity)).thenReturn(outEntity);

        var result = sut.create(inDto);

        verify(service).save(inEntity);
        assertThat(result, is(outDto));
    }

    @Test
    void deleteWhenNotAllowedThrowsException() {
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isDevices()).thenReturn(false);

        var exception = assertThrows(FeatureIsDisabledException.class, () -> sut.delete("name"));

        assertThat(exception.getMessage(), is("Feature editing of device is not enabled"));
    }

    @Test
    void deleteWhenAllowedCallsService() throws FeatureIsDisabledException, EntityNotFoundException {
        String deviceName = "name";
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isDevices()).thenReturn(true);

        sut.delete(deviceName);

        verify(service).deleteByName(deviceName);
    }
}