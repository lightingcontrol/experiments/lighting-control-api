package io.gitlab.lightingcontrol.api.controller;

import io.gitlab.lightingcontrol.api.configuration.ConfigProperties;
import io.gitlab.lightingcontrol.api.dto.ChannelDto;
import io.gitlab.lightingcontrol.api.entity.Channel;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.exception.FeatureIsDisabledException;
import io.gitlab.lightingcontrol.api.mapper.ChannelMapper;
import io.gitlab.lightingcontrol.api.service.ChannelService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ChannelControllerTest {

    @InjectMocks
    private ChannelController sut;
    @Mock
    private ChannelService service;
    @Mock
    private ChannelMapper mapper;
    @Mock
    private ConfigProperties configProperties;
    @Mock
    private ConfigProperties.EditingProperties editingProperties;

    @Test
    void indexFindAndMapsResult() {
        var channel = new Channel("name", "type");
        var channelDto = new ChannelDto("name", "type");
        when(service.findAll()).thenReturn(new PageImpl<>(List.of(channel)));
        when(mapper.toDto(channel)).thenReturn(channelDto);

        var result = sut.index();

        verify(service).findAll();
        verify(mapper).toDto(channel);
        assertThat(result, allOf(
                hasProperty("content", hasItem(channelDto)),
                hasProperty("size", is(1)),
                hasProperty("totalPages", is(1)),
                hasProperty("totalElements", is(1L))));
    }

    @Test
    void createWhenNotAllowedThrowsException() {
        var channelDto = new ChannelDto("name", "type");
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isChannels()).thenReturn(false);

        var exception = assertThrows(FeatureIsDisabledException.class, () -> sut.create(channelDto));

        assertThat(exception.getMessage(), is("Feature editing of channel is not enabled"));
    }

    @Test
    void createWhenAllowedCallsService() throws FeatureIsDisabledException {
        var inDto = new ChannelDto("in", "in");
        var inEntity = new Channel("in", "in");
        var outDto = new ChannelDto("out", "out");
        var outEntity = new Channel("out", "out");
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isChannels()).thenReturn(true);
        when(mapper.toEntity(inDto)).thenReturn(inEntity);
        when(mapper.toDto(outEntity)).thenReturn(outDto);
        when(service.save(inEntity)).thenReturn(outEntity);

        var result = sut.create(inDto);

        verify(service).save(inEntity);
        assertThat(result, is(outDto));
    }

    @Test
    void deleteWhenNotAllowedThrowsException() {
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isChannels()).thenReturn(false);

        var exception = assertThrows(FeatureIsDisabledException.class, () -> sut.delete("name"));

        assertThat(exception.getMessage(), is("Feature editing of channel is not enabled"));
    }

    @Test
    void deleteWhenAllowedCallsService() throws FeatureIsDisabledException, EntityNotFoundException {
        String channelName = "name";
        when(configProperties.getEditing()).thenReturn(editingProperties);
        when(editingProperties.isChannels()).thenReturn(true);

        sut.delete(channelName);

        verify(service).deleteByName(channelName);
    }
}