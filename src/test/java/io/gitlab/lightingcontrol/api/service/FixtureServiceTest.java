package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Fixture;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.FixtureRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FixtureServiceTest {

    @InjectMocks
    private FixtureService sut; // System Under Test

    @Mock
    private FixtureRepository repository;

    @Test
    void findAllShouldForwardToRepository() {
        var expectedResult = new PageImpl<Fixture>(List.of());
        when(repository.findAll(any(Pageable.class))).thenReturn(expectedResult);

        var result = sut.findAll();

        verify(repository).findAll(Pageable.unpaged());
        assertThat(result, is(expectedResult));
    }

    @Test
    void saveShouldForwardToRepository() {
        var param = new Fixture("name", "type", List.of());
        when(repository.save(any(Fixture.class))).thenReturn(param);

        var result = sut.save(param);

        verify(repository).save(param);
        assertThat(result, is(param));
    }

    @Test
    void deleteByNameThrowsExceptionIfNotFound() {
        String notFoundName = "notFound";
        when(repository.findById(notFoundName)).thenReturn(Optional.empty());

        var exception = assertThrows(EntityNotFoundException.class, () ->
                sut.deleteByName(notFoundName));

        assertThat(exception.getMessage(), is("class io.gitlab.lightingcontrol.api.entity.Fixture of notFound could not be found"));
    }

    @Test
    void deleteByNameDeletesIfFound() throws EntityNotFoundException {
        var foundName = "found";
        var foundFixture = new Fixture("found", "found", List.of());
        when(repository.findById(foundName)).thenReturn(Optional.of(foundFixture));

        sut.deleteByName(foundName);

        verify(repository).delete(foundFixture);
    }
}