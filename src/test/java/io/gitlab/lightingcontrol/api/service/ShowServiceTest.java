package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Show;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.ShowRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShowServiceTest {

    @InjectMocks
    private ShowService sut; // System Under Test

    @Mock
    private ShowRepository repository;

    @Test
    void findAllShouldForwardToRepository() {
        var page = Pageable.ofSize(13);
        var expectedResult = new PageImpl<Show>(List.of());
        when(repository.findAll(any(Pageable.class))).thenReturn(expectedResult);

        var result = sut.findAll(page);

        verify(repository).findAll(page);
        assertThat(result, is(expectedResult));
    }

    @Test
    void findShouldForwardToRepository() {
        var param = "name";
        var expectedResult = Optional.<Show>empty();
        when(repository.findById(any(String.class))).thenReturn(expectedResult);

        var result = sut.find(param);

        verify(repository).findById(param);
        assertThat(result, is(expectedResult));
    }

    @Test
    void saveShouldForwardToRepository() {
        var param = new Show("name", List.of());
        when(repository.save(any(Show.class))).thenReturn(param);

        var result = sut.save(param);

        verify(repository).save(param);
        assertThat(result, is(param));
    }

    @Test
    void deleteByNameThrowsExceptionIfNotFound() {
        String notFoundName = "notFound";
        when(repository.findById(notFoundName)).thenReturn(Optional.empty());

        var exception = assertThrows(EntityNotFoundException.class, () ->
                sut.deleteByName(notFoundName));

        assertThat(exception.getMessage(), is("class io.gitlab.lightingcontrol.api.entity.Scene of notFound could not be found"));
    }

    @Test
    void deleteByNameDeletesIfFound() throws EntityNotFoundException {
        var foundName = "found";
        var foundScene = new Show("found", List.of());
        when(repository.findById(foundName)).thenReturn(Optional.of(foundScene));

        sut.deleteByName(foundName);

        verify(repository).delete(foundScene);
    }
}