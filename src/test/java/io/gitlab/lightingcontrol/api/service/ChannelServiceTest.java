package io.gitlab.lightingcontrol.api.service;

import io.gitlab.lightingcontrol.api.entity.Channel;
import io.gitlab.lightingcontrol.api.exception.EntityNotFoundException;
import io.gitlab.lightingcontrol.api.repository.ChannelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ChannelServiceTest {

    // System Under Test
    @InjectMocks
    private ChannelService sut;

    @Mock
    private ChannelRepository repository;

    @Captor
    private ArgumentCaptor<Pageable> pageableArgumentCaptor;

    @Test
    void findAllForwardsUnpagedQueryToRepository() {
        sut.findAll();

        verify(repository).findAll(pageableArgumentCaptor.capture());
        var pageable = pageableArgumentCaptor.getValue();
        assertThat(pageable, is(Pageable.unpaged()));
    }

    @Test
    void saveForwardsUnpagedQueryToRepository() {
        var outChannel = new Channel("out", "out");
        var inChannel = new Channel("in", "in");
        when(repository.save(inChannel)).thenReturn(outChannel);

        var result = sut.save(inChannel);

        verify(repository).save(inChannel);
        assertThat(result, is(outChannel));
    }

    @Test
    void deleteByNameThrowsExceptionIfNotFound() {
        String notFoundName = "notFound";
        when(repository.findById(notFoundName)).thenReturn(Optional.empty());

        var exception = assertThrows(EntityNotFoundException.class, () ->
                sut.deleteByName(notFoundName));

        assertThat(exception.getMessage(), is("class io.gitlab.lightingcontrol.api.entity.Channel of notFound could not be found"));
    }

    @Test
    void deleteByNameDeletesIfFound() throws EntityNotFoundException {
        var foundName = "found";
        var foundChannel = new Channel("found", "found");
        when(repository.findById(foundName)).thenReturn(Optional.of(foundChannel));

        sut.deleteByName(foundName);

        verify(repository).delete(foundChannel);
    }
}